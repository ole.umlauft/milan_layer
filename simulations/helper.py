from keras import regularizers
from keras.layers import Input, LSTM, TimeDistributed, Dense
from keras.models import Model
from keras.utils import plot_model

import numpy as np
import os

def parse_model(model_str, input_layer_shape=(None, 2)):
    x = Input(shape=input_layer_shape)
    kernel_regularizer = regularizers.l2(0.001)
    bias_regularizer = regularizers.l2(0.001)

    start = True
    for layer in list(model_str.split('x')):
        if start == True:
            x_net = x
            start = False

        if layer[0] == 'l':
            layer = int(layer[1:])
            x_net = LSTM(layer, return_sequences=True)(x_net)
        else:
            layer = int(layer)
            x_net = TimeDistributed(Dense(layer, activation='relu', kernel_regularizer=kernel_regularizer,
                                          bias_regularizer=bias_regularizer))(x_net)

    out = TimeDistributed(
        Dense(4, activation='linear', kernel_regularizer=kernel_regularizer, bias_regularizer=bias_regularizer))(x_net)

    return Model(x, out)


def init_model_dir(path, model, savetime):
    os.mkdir('{}/model_{}'.format(path, savetime))
    checkpoint_path = '{}/model_{}/checkpoints/'.format(path, savetime)
    os.mkdir(checkpoint_path)

    model_json = model.to_json()
    with open("{}/model_{}/model_{}.json".format(path, savetime, savetime), "w") as json_file:
        json_file.write(model_json)
    return checkpoint_path


def save_trained_model(path, model, history, savetime):
    plot_model(model, to_file='{}/model_{}/model_{}.png'.format(path, savetime, savetime), show_shapes=True)

    model.save_weights("{}/model_{}/model_{}.h5".format(path, savetime, savetime))

    numpy_loss_history = np.array(history.history["loss"])
    numpy_val_loss_history = np.array(history.history["val_loss"])
    np.savetxt("{}/model_{}/loss_history_{}.txt".format(path, savetime, savetime), numpy_loss_history, delimiter=",")
    np.savetxt("{}/model_{}/val_loss_history_{}.txt".format(path, savetime, savetime), numpy_val_loss_history,
               delimiter=",")

