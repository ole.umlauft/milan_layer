import matplotlib.pyplot as plt
import numpy as np
import os
import pickle

from keras.layers import Input, LSTM, Dense, TimeDistributed
from keras.models import Model
from numpy.linalg import norm
from tqdm import tqdm

from testing.kalman_filter import kf_predict, kf_update
from testing.constant_velocity_model import batch_generator


def kalman_filter(measurements, dt):
    kalman_batches = []
    for ms_batch in tqdm(measurements):
        kalman_trajectories = []
        for ms in ms_batch:

            X = np.array([[0.0], [0.0], [dt], [dt]])
            P = np.diag((dt, dt, dt, dt))
            A = np.array([[1.0, 0.0, dt, 0.0],
                          [0.0, 1.0, 0.0, dt],
                          [0.0, 0.0, 1.0, 0.0],
                          [0.0, 0.0, 0.0, 1.0]])
            Q = np.array([[0.01, 0.  , 0.02, 0.  ],
                          [0.  , 0.01, 0.  , 0.02],
                          [0.02, 0.  , 0.04, 0.  ],
                          [0.  , 0.02, 0.  , 0.04]])#np.diag((0.1, 0.1, 0.01, 0.01))
            B = np.eye(X.shape[0])
            U = np.zeros((X.shape[0], 1))

            ms = np.reshape(ms, newshape=(ms.shape[0], ms.shape[1], 1))
            H = np.array([[1, 0, 0, 0],
                          [0, 1, 0, 0],
                          [0, 0, 1, 0],
                          [0, 0, 0, 1]])
            R = np.diag((0.1, 0.1, 0.01, 0.01))  # np.eye(ms.shape[1])

            updated = []
            for position in range(ms.shape[0]):
                (X, P) = kf_predict(X, P, A, Q, B, U)
                (X, P, K, IM, IS, LH) = kf_update(X, P, ms[position], H, R)
                updated.append(np.reshape(X, (X.shape[0], X.shape[1])))
            kalman_trajectories.append(updated)
        kalman_batches.append(kalman_trajectories)
    return np.array(kalman_batches)

def lstm_filter(model, measurements):
    lstm_batches = []
    for batch in measurements:
        lstm_estimation = model.predict(batch, batch_size=100)
        lstm_batches.append(lstm_estimation)
    return np.array(lstm_batches)

def mse(estimation, ground_truth):
    estimation = np.reshape(estimation, (estimation.shape[0]*estimation.shape[1],
                                         estimation.shape[2],
                                         estimation.shape[3]))

    ground_truth = np.reshape(ground_truth, (ground_truth.shape[0]*ground_truth.shape[1],
                                             ground_truth.shape[2],
                                             ground_truth.shape[3]))
    mse = norm(ground_truth-estimation, axis=-1)
    mse = np.sum(mse, axis=0)/ground_truth.shape[0]
    return mse



if __name__ == "__main__":
    dt = 1
    steps = 20
    batch_size = 100

    data_path = "data/kf_rnn/"
    if not os.path.exists(data_path):
        os.mkdir(data_path)

    if (not os.path.exists(f"{data_path}/training_tj.pkl")) or \
        (not os.path.exists((f"{data_path}/training_ms.pkl"))):
        training_tj = []
        training_ms = []

        g_tj = batch_generator(n=500,
                               batch_size=batch_size,
                               init=np.array([0, 0, 1, 1]),
                               init_cov=np.identity(4) * 0.05 ** 2,
                               sigma_q=np.array([0.1, 0.1]) ** 2,
                               m_cov=np.diag((0.1, 0.1, 0.01, 0.01)),
                               dt=dt, steps=steps)
        for i in tqdm(range(500)):
            tj, ms = next(g_tj)
            training_tj += [tj]
            training_ms += [ms]
        pickle.dump(training_tj, open(f"{data_path}/training_tj.pkl", "wb"))
        pickle.dump(training_ms, open(f"{data_path}/training_ms.pkl", "wb"))

    else:
        training_tj = pickle.load(open(f"{data_path}/training_tj.pkl", "rb"))
        training_ms = pickle.load(open(f"{data_path}/training_ms.pkl", "rb"))

    training_tj = np.array(training_tj)
    training_ms = np.array(training_ms)


    if (not os.path.exists(f"{data_path}/testing_tj.pkl")) or \
        (not os.path.exists((f"{data_path}/testing_ms.pkl"))):
        testing_tj = []
        testing_ms = []

        g_ms = batch_generator(n=100,
                               batch_size=batch_size,
                               init=np.array([0, 0, 1, 1]),
                               init_cov=np.identity(4) * 0.05 ** 2,
                               sigma_q=np.array([0.1, 0.1]) ** 2,
                               m_cov=np.diag((0.1, 0.1, 0.01, 0.01)),
                               dt=dt, steps=steps)

        for i in tqdm(range(100)):
            tj, ms = next(g_ms)
            testing_tj += [tj]
            testing_ms += [ms]
        pickle.dump(testing_tj, open(f"{data_path}/testing_tj.pkl", "wb"))
        pickle.dump(testing_ms, open(f"{data_path}/testing_ms.pkl", "wb"))

    else:
        testing_tj = pickle.load(open(f"{data_path}/testing_tj.pkl", "rb"))
        testing_ms = pickle.load(open(f"{data_path}/testing_ms.pkl", "rb"))

    testing_tj = np.array(testing_tj)
    testing_ms = np.array(testing_ms)

    if not os.path.exists(f"{data_path}/kalman_estimations.pkl"):
        kalman_estimations = kalman_filter(testing_ms, dt)
        pickle.dump(kalman_estimations, open(f"{data_path}/kalman_estimations.pkl", "wb"))
    else:
        kalman_estimations = pickle.load(open(f"{data_path}/kalman_estimations.pkl", "rb"))

    if not os.path.exists(f"{data_path}/lstm_estimations.pkl"):
        input = Input((None, 4))
        lstm = LSTM(10, return_sequences=True)(input)
        dense = TimeDistributed(Dense(10, activation="relu"))(lstm)
        dense = TimeDistributed(Dense(100, activation="relu"))(dense)
        dense = TimeDistributed(Dense(4, activation="linear"))(dense)
        model = Model(input, dense)
        model.compile(optimizer="adam", loss="mean_squared_error")

        if not os.path.exists(f"{data_path}/lstm_model_weights.h5"):
            history = model.fit(
                np.reshape(training_ms, (training_ms.shape[0]*training_ms.shape[1], training_ms.shape[2], training_ms.shape[3])),
                np.reshape(training_tj, (training_tj.shape[0]*training_tj.shape[1], training_tj.shape[2], training_tj.shape[3])),
                batch_size=batch_size, epochs=20, validation_split=0.1)
            model.save_weights(f"{data_path}/lstm_model_weights.h5", "w")
        else:
            model.load_weights(f"{data_path}/lstm_model_weights.h5")

        lstm_estimations = lstm_filter(model, testing_ms)
        pickle.dump(lstm_estimations, open(f"{data_path}/lstm_estimations.pkl", "wb"))
    else:
        lstm_estimations = pickle.load(open(f"{data_path}/lstm_estimations.pkl", "rb"))

    input = Input((None, 4))
    lstm = LSTM(10, return_sequences=True)(input)
    dense = TimeDistributed(Dense(10, activation="relu"))(lstm)
    dense = TimeDistributed(Dense(100, activation="relu"))(dense)
    dense = TimeDistributed(Dense(4, activation="linear"))(dense)
    model = Model(input, dense)
    model.compile(optimizer="adam", loss="mean_squared_error")

    examples = [(2, 50), (52, 72)]
    for (x, y) in examples:

        plt.figure(1)
        plt.plot(testing_tj[x, y, :, 0], testing_tj[x, y, :, 1], label="ground truth")
        plt.scatter(testing_tj[x, y, :, 0], testing_tj[x, y, :, 1])
        plt.plot(testing_ms[x, y, :, 0], testing_ms[x, y, :, 1], label="measurements")
        plt.scatter(testing_ms[x, y, :, 0], testing_ms[x, y, :, 1])
        plt.plot(lstm_estimations[x, y, :, 0], lstm_estimations[x, y, :, 1], label="lstm")
        plt.scatter(lstm_estimations[x, y, :, 0], lstm_estimations[x, y, :, 1])
        plt.plot(kalman_estimations[x, y, :, 0], kalman_estimations[x, y, :, 1], label="kalman")
        plt.scatter(kalman_estimations[x, y, :, 0], kalman_estimations[x, y, :, 1])
        plt.legend(loc='upper left', prop={'size': 10})
        plt.show()


    mse_ms = mse(testing_ms, testing_tj)
    mse_kf = mse(kalman_estimations, testing_tj)
    mse_lstm = mse(lstm_estimations, testing_tj)

    plt.figure(1)
    plt.plot(mse_ms, label="measurements")
    plt.plot(mse_kf, label="kalman filter")
    plt.plot(mse_lstm, label="lstm filter")
    plt.legend(loc='upper left', prop={'size': 10})
    plt.show()
