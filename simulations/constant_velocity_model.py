import numpy as np
from numpy.random import multivariate_normal as mvn
import matplotlib.pyplot as plt


def get_data(init, init_cov, sigma_q, m_cov, dt, steps):
    '''
    Generates a trajectory and a measurement for each time step.
    :param init:        expected initial position and velocity of track (x, y, vx, vy)
    :param init_cov:    uncertainty of initial position (4,4)
    :param sigma_q:     transition noise on x- and y-velocity
    :param dt:          length of a time step
    :param m_cov:       measurement noise covariance (2,2)
    :param steps:       length of trajectory
    '''
    # trajectory setup
    traj = np.zeros((steps, 4))
    traj[0] = init + mvn(np.zeros(4), init_cov)  # initial position
    trans = np.array([[1, 0, dt, 0],
                      [0, 1, 0, dt],
                      [0, 0, 1, 0],
                      [0, 0, 0, 1]])  # transition matrix
    g = np.array([[dt ** 2 / 2, 0],
                  [0, dt ** 2 / 2],
                  [dt, 0],
                  [0, dt]])

    trans_cov = np.einsum('ab, bc, dc -> ad', g, np.diag(sigma_q), g)  # transition error covariance

    print(trans_cov)
    # measurement setup
    ms = np.zeros((steps, 4))
    ms[0] = traj[0] + mvn(np.zeros(4), m_cov)

    # generate move object and generate measurement
    for i in range(1, steps):
        traj[i] = np.dot(trans, traj[i - 1])
        traj[i] += mvn(np.zeros(4), trans_cov)
        ms[i] = traj[i] + mvn(np.zeros(4), m_cov)

    return traj, ms


def get_batch(batch_size, init, init_cov, sigma_q, m_cov, dt, steps):
    batch_ms = np.zeros((batch_size, steps, 4))
    batch_traj = np.zeros((batch_size, steps, 4))
    for i in range(batch_size):
        batch_traj[i], batch_ms[i] = get_data(init, init_cov, sigma_q, m_cov, dt, steps)
    return batch_traj, batch_ms


def batch_generator(n, batch_size, init, init_cov, sigma_q, m_cov, dt, steps):
    i = 0
    while i < n:
        yield get_batch(batch_size, init, init_cov, sigma_q, m_cov, dt, steps)
        i += 1
