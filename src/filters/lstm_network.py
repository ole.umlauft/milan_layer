from keras.models import Model
from keras.layers import LSTM, Input, Dense, TimeDistributed

def load_model(weights_file_path, input_shape=(None, 4)):
    input = Input(shape=input_shape) # or (None, 2) if range and bearing model
    lstm = LSTM(units=20, return_sequences=True)(input) # 20 units for 20 timesteps
    dense = TimeDistributed(Dense(10, activation='relu'))(lstm)
    dense = TimeDistributed(Dense(10, activation='relu'))(dense)
    out = TimeDistributed(Dense(4, activation='linear'))(dense)

    model = Model(input, out)
    model.compile(optimizer='adam', loss='mean_squared_error')
    model.load_weights(weights_file_path)
    return model
