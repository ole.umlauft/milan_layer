from keras.models import Model
from keras.layers import Input, Dense, TimeDistributed

from src.rnn import ModifiedRNN

def load_model(weights_file_path, input_shape=(None, 4)):
    input = Input(shape=input_shape) # or (None, 2) if range and bearing model
    rnn = ModifiedRNN(units=4, return_sequences=True)(input)
    
    estimated = Lambda(lambda x: x[:, :, 0:4])(rnn)
    predicted = Lambda(lambda y: y[:, :, 4:8])(rnn)
    
    estimated = TimeDistributed(Dense(10, activation='relu'))(estimated)
    predicted = TimeDistributed(Dense(10, activation='relu'))(predicted)
    
    estimated = TimeDistributed(Dense(4, activation='linear'), name='updated_output')(estimated)
    predicted = TimeDistributed(Dense(4, activation='linear'), name='predicted_output')(predicted)
    
    model = Model(input, [estimated, predicted])
    losses = {
        'updated_output': 'mean_squared_error',
        'predicted_output': 'mean_squared_error'
    }
    model.compile(optimizer='adam', loss=losses)
    model.load_weights(weights_file_path)
    return model
