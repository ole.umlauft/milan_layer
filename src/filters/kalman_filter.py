import numpy as np
import matplotlib.pyplot as plt

from testing.constant_velocity_model import get_data

'''
This code based on this "Implementation of Kalman Filter with Python Language"
https://arxiv.org/pdf/1204.0375.pdf
'''

def kf_predict(X, P, A, Q, B, U):
    '''
    Kalman filter prediction step.
    :param X: The mean state estimate of the previous step (k-1).
    :param P: The state covariance of the previous step (k-1).
    :param A: The transition n x n matrix.
    :param Q: The process noise covariance matrix.
    :param B: The input effect matrix.
    :param U: The control Input.
    :return: (predicted state, predicted state covariance)
    '''
    X = np.dot(A, X) + np.dot(B, U)
    P = np.dot(A, np.dot(P, A.T)) + Q
    return (X, P)

def kf_update(X, P, Y, H, R):
    '''
    Kalman filter update step.
    Additional parameters:
    K -- The kalman gain matrix.
    IM -- The mean of predictive distribution of Y.
    IS -- The Covariance or predictive mean of Y.
    LH -- The predictive probability (likelihood)
          of measurement which is computed using the
          function gauss_pdf.

    :param X: The mean state estimate.
    :param P: The state covariance.
    :param Y: The measurement vector.
    :param H: The measurement matrix H.
    :param R: The measurement covariance matrix.
    :return:

    '''
    IM = np.dot(H, X)
    IS = R + np.dot(H, np.dot(P, H.T))
    K = np.dot(P, np.dot(H.T, np.linalg.inv(IS)))
    X = X + np.dot(K, (Y-IM))
    P = P - np.dot(K, np.dot(IS, K.T))
    LH = gauss_pdf(Y, IM, IS)
    return (X, P, K, IM, IS, LH)

def gauss_pdf(X, M, S):
    '''
    Gauss pdf
    :param X: The measurement vector.
    :param M: The mean of predictive distribution of Y.
    :param S:
    :return:
    '''
    if M.shape[1] == 1:
        DX = X - np.tile(M, X.shape[1])
        E = 0.5 * np.sum(DX * (np.dot(np.linalg.inv(S), DX)), axis=0)
    elif X.shape[1] == 1:
        DX = np.tile(X, M.shape[1]) - M
        E = 0.5 * np.sum(DX * (np.dot(np.linalg.inv(S), DX)), axis=0)
    else:
        DX = X - M
        E = 0.5 * np.dot(DX.T, np.dot(np.linalg.inv(S), DX))

    E = E + 0.5 * M.shape[0] * np.log(2 * np.pi) + 0.5 * np.log(np.linalg.det(S))
    P = np.exp(-E)
    return (P[0], E[0])


def _min_of_three(a, b, c):
    return np.min([np.min([np.min(a), np.min(b)]), np.min(c)])

def _max_of_three(a, b, c):
    return np.max([np.max([np.max(a), np.max(b)]), np.max(c)])

