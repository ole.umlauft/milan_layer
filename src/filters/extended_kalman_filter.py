import numpy as np
from tqdm import tqdm

def hF(vX, mF):
    '''
    System model function.
    :param vX: last state
    :param mF: transition matrix F
    :return: Returns the next state.
    '''
    vX = np.dot(mF, vX)
    return vX

def hH(vX, sensor):
    '''
    Measurement model function.
    :param vX: state
    :param sensor: sensor's position
    :return: Returns the distance and angle for a state x.
    '''
    x, y, _, _ = vX
    x -= sensor[0]
    y -= sensor[1]
    distance = np.linalg.norm(np.array([x, y]))
    angle = np.arctan2(y, x)
    return np.array([distance, angle])


def jacobian(vX, sensor):
    '''
    Jacobian matrix which replaces the measurement matrix.
    :param vX: state
    :param sensor: sensor's position
    :return: Returns the Jacobian matrix for state x.
    '''
    x, y, _, _ = vX
    x -= sensor[0]
    y -= sensor[1]
    return np.array([[x / np.linalg.norm([x, y]), y / np.linalg.norm([x, y]), 0, 0],
                     [(-1 * y) / (x ** 2 + y ** 2), x / (x ** 2 + y ** 2), 0, 0]])


def normalize_angle(a):
    '''
    Normalizes the calculated angle.
    :param a:
    :return: Returns the normalized angle.
    '''
    if a >= 0 and a < 2 * np.pi:
        return a
    elif a < 0:
        return normalize_angle(a + 2*np.pi)
    else:
        return normalize_angle(a - 2*np.pi)

def predict_ekf(vX, mP, mF, mQ, hF):
    '''
    Prediction step.
    :param vX: The mean state estimate of the previous step (k-1).
    :param mP: The state covariance of the previous step (k-1).
    :param mF: The transition n x n matrix.
    :param mQ: The process noise covariance matrix.
    :param hF: The system model function.
    :return: Returns the predicted state and the state covariance matrix.
    '''
    vX = hF(vX, mF)
    mP = np.dot(mF, mP.dot(mF.T)) + mQ
    return vX, mP


def update_ekf(vZ, vX, mP, mI, mR, hH, jac, sensor):
    '''
    Update step.
    :param vZ: The measurement vector.
    :param vX: The mean state estimate.
    :param mP: The state covariance.
    :param mI: The identity matrix.
    :param mR: The measurement covariance matrix.
    :param jac: The Jacobian measurement matrix.

    :param sensor: sensor position
    :return: Returns the updated estimated state and updated state error covariance matrix.
    '''
    mH = jac(vX, sensor)
    vY = np.subtract(vZ, hH(vX, sensor))
    vY[1] = normalize_angle(vY)
    mS = np.dot(mH, np.dot(mP, mH.T)) + mR
    mK = np.dot(np.dot(mP, mH.T), np.linalg.inv(mS))
    vX = vX + np.dot(mK, vY)
    mT = mI - np.dot(mK, mH)
    mP = (np.dot(mT, mP).dot(mT.T)) + (np.dot(mK, mR).dot(mK.T))  # Joseph Form
    return vX, mP


def extended_kalman_filter(
        ms,
        sensor,
        mF=np.array([[1, 0, 1, 0],
                     [0, 1, 0, 1],
                     [0, 0, 1, 0],
                     [0, 0, 0, 1]]),
        mP=np.diag([1.0, 1.0, 0.1, 0.1]),
        mQ=np.array([[0.01, 0., 0.02, 0.],
                     [0., 0.01, 0., 0.02],
                     [0.02, 0., 0.04, 0.],
                     [0., 0.02, 0., 0.04]]),
        mR=np.array([[0.01, 0], [0, 0.002]]),
        mI=np.diag((1.0, 1.0, 1.0, 1.0))):
        '''
        The extended Kalman filter.
        :param ms: A timeseries.
        :param sensor: The sensor position.
        :param mF: The transition n x n matrix.
        :param mP: The state covariance of the previous step (k-1).
        :param mQ: The process noise covariance matrix.
        :param mR: The measurement covariance matrix.
        :param mI: The identity matrix.
        :return: Returns the updated or estimated timeseries.
        '''

    vX_0 = np.array([np.cos(ms[0, 1]), np.sin(ms[0, 1])]).T * ms[0, 0, None] + sensor

    new_vX = np.ones(4)
    new_vX[0] = vX_0[0]
    new_vX[1] = vX_0[1]
    new_vX[2] = 1 # x-velocity
    new_vX[3] = 1 # y-velocity
    vX = new_vX

    updated = [vX]
    for position in range(1, ms.shape[0]):
        (vX, mP) = predict_ekf(vX, mP, mF, mQ, hF)
        (vX, mP) = update_ekf(ms[position], vX, mP, mI, mR, hH, jacobian, sensor)
        updated.append(vX)
    return updated


def batch_ekf(batches, sensor):
    updated = []
    for ms in tqdm(batches):
        updated.append(extended_kalman_filter(ms, sensor))
    return np.array(updated)