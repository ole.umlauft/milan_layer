import keras.backend as K
from keras.layers import Layer, RNN

from keras import activations, initializers, regularizers, constraints
from keras.legacy import interfaces


class ModifiedRNNCell(Layer):
    """Cell class for the ModifiedRNN layer.

    # Arguments
        units: Positive integer, dimensionality of the output space.
        activation: Activation function to use (see [activations](https://keras.io/activations/)).
            Default: hyperbolic tangent (`tanh`).
        use_bias: Boolean, whether the layer uses a bias vector.
        kernel_initializer: Initializer for the `kernel` weights matrix,
            used for the linear transformation of the inputs
            (see [initializers](https://keras.io/initializers/)).
        kernel_regularizer: Regularizer function applied to
            the `kernel` weights matrix
            (see [regularizer](https://keras.io/regularizers/)).
        kernel_constraint: Constraint function applied to
            the `kernel` weights matrix
            (see [constraints](https://keras.io/constraints/)).
        bias_initializer: Initializer for the `bias` vector
            (see [initializers](https://keras.io/initializers/)).
        bias_regularizer: Regularizer function applied to the `bias` vector
            (see [regularizer](https://keras.io/regularizers/)).
        bias_constraint: Constraint function applied to the `bias` vector
            (see [constraints](https://keras.io/constraints/)).
    """

    def __init__(self,
                 units,
                 activation='tanh',
                 use_bias=True,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 kernel_constraint=None,
                 bias_initializer='zeros',
                 bias_regularizer=None,
                 bias_constraint=None,
                 **kwargs):
        self.units = units
        self.activation = activations.get(activation)
        self.use_bias = use_bias
        self.kernel_initializer = initializers.get(kernel_initializer)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.kernel_constraint = constraints.get(kernel_constraint)
        self.bias_initializer = initializers.get(bias_initializer)
        self.bias_regularizer = regularizers.get(bias_regularizer)
        self.bias_constraint = constraints.get(bias_constraint)

        self.state_size = (self.units, self.units, self.units)
        self.output_size = self.units*2

        super(ModifiedRNNCell, self).__init__(**kwargs)

    def build(self, input_shape):
        self.kernel = self.add_weight(name='kernel',
                                      shape=(self.units, self.units*4),
                                      initializer=self.kernel_initializer,
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint,
                                      trainable=True)

        self.concat_kernel = self.add_weight(name='concat_kernel',
                                      shape=(input_shape[-1]+self.units, self.units),
                                      initializer=self.kernel_initializer,
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint,
                                      trainable=True)

        self.kernel_x = self.kernel[:, :self.units]
        self.kernel_h = self.kernel[:, self.units:self.units*2]
        self.kernel_p = self.kernel[:, self.units*2:self.units*3]
        self.kernel_u = self.kernel[:, self.units*3:self.units*4]

        if self.use_bias:
            self.bias = self.add_weight(shape=(self.units * 3,),
                                        name='bias',
                                        initializer=self.bias_initializer,
                                        regularizer=self.bias_regularizer,
                                        constraint=self.bias_constraint)
        else:
            self.bias = None

        if self.use_bias:
            self.bias_x = self.bias[:self.units]
            self.bias_p = self.bias[self.units:self.units*2]
            self.bias_u = self.bias[self.units*2:self.units*3]
        else:
            self.bias_x = None
            self.bias_p = None
            self.bias_u = None

        self.built = True

    def call(self, inputs, states, training=None, **kwargs):
        y = inputs
        h = states[1]  # hidden state
        p = states[2]  # predicted state

        x = K.concatenate([p, y], axis=-1)
        x = K.dot(x, self.concat_kernel)
        x = x + h
        x = self.activation(x)
        x = K.dot(x, self.kernel_u)  # estimated state

        h = K.dot(x, self.kernel_x) + K.dot(h, self.kernel_h)
        if self.use_bias:
            h = K.bias_add(h, self.bias_x)
        h = self.activation(h)

        p = K.dot(h, self.kernel_p)
        if self.use_bias:
            p = K.bias_add(p, self.bias_p)
        return K.concatenate([x, p], axis=-1), [x, h, p]

    def get_config(self):
        config = {'units': self.units,
                  'activation': activations.serialize(self.activation),
                  'use_bias': self.use_bias,
                  'kernel_initializer': initializers.serialize(self.kernel_initializer),
                  'kernel_regularizer': regularizers.serialize(self.kernel_regularizer),
                  'kernel_constraint': constraints.serialize(self.kernel_constraint),
                  'bias_initializer': initializers.serialize(self.bias_initializer),
                  'bias_regularizer': regularizers.serialize(self.bias_regularizer),
                  'bias_constraint': constraints.serialize(self.bias_constraint)}
        base_config = super(ModifiedRNNCell, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class ModifiedRNN(RNN):
    """Modified RNN class of the RNN approach of Anton Milan et. al. - 2016.

    # Arguments
        units: Positive integer, dimensionality of the output space.
        activation: Activation function to use (see [activations](https://keras.io/activations/)).
            Default: hyperbolic tangent (`tanh`).
        use_bias: Boolean, whether the layer uses a bias vector.
        kernel_initializer: Initializer for the `kernel` weights matrix,
            used for the linear transformation of the inputs
            (see [initializers](https://keras.io/initializers/)).
        kernel_regularizer: Regularizer function applied to
            the `kernel` weights matrix
            (see [regularizer](https://keras.io/regularizers/)).
        kernel_constraint: Constraint function applied to
            the `kernel` weights matrix
            (see [constraints](https://keras.io/constraints/)).
        bias_initializer: Initializer for the `bias` vector
            (see [initializers](https://keras.io/initializers/)).
        bias_regularizer: Regularizer function applied to the `bias` vector
            (see [regularizer](https://keras.io/regularizers/)).
        bias_constraint: Constraint function applied to the `bias` vector
            (see [constraints](https://keras.io/constraints/)).
        return_sequences: Boolean. Whether to return the last output
            in the output sequence, or the full sequence.
        return_state: Boolean. Whether to return the last state
            in addition to the output. The returned elements of the
            states list are the hidden state and the cell state, respectively.
        go_backwards: Boolean (default False).
            If True, process the input sequence backwards and return the
            reversed sequence.
        stateful: Boolean (default False). If True, the last state
            for each sample at index i in a batch will be used as initial
            state for the sample of index i in the following batch.
        unroll: Boolean (default False).
            If True, the network will be unrolled,
            else a symbolic loop will be used.
            Unrolling can speed-up a RNN,
            although it tends to be more memory-intensive.
            Unrolling is only suitable for short sequences.
    # References
        - [Online Multi-Target Tracking Using Recurrent Neural Networks](https://arxiv.org/pdf/1604.03635.pdf)

    """
    @interfaces.legacy_recurrent_support
    def __init__(self,
                 units,
                 activation='tanh',
                 use_bias=True,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 kernel_constraint=None,
                 bias_initializer='zeros',
                 bias_regularizer=None,
                 bias_constraint=None,
                 return_sequences=False,
                 return_state=False,
                 go_backwards=False,
                 stateful=False,
                 unroll=False,
                 **kwargs):

        cell = ModifiedRNNCell(units=units,
                               activation=activation,
                               use_bias=use_bias,
                               kernel_initializer=kernel_initializer,
                               kernel_regularizer=kernel_regularizer,
                               kernel_constraint=kernel_constraint,
                               bias_initializer=bias_initializer,
                               bias_regularizer=bias_regularizer,
                               bias_constraint=bias_constraint)

        super(ModifiedRNN, self).__init__(
            cell=cell,
            return_sequences=return_sequences,
            return_state=return_state,
            go_backwards=go_backwards,
            stateful=stateful,
            unroll=unroll,
            **kwargs)

    def call(self, inputs, mask=None, training=None, initial_state=None, **kwargs):
        return super(ModifiedRNN, self).call(inputs=inputs,
                                             mask=mask,
                                             training=training,
                                             initial_state=initial_state)

    @property
    def units(self):
        return self.cell.units

    @property
    def activation(self):
        return self.cell.activation

    @property
    def use_bias(self):
        return self.cell.use_bias

    @property
    def kernel_initializer(self):
        return self.cell.kernel_initializer

    @property
    def kernel_regularizer(self):
        return self.cell.kernel_regularizer

    @property
    def kernel_constraint(self):
        return self.cell.kernel_constraint

    @property
    def bias_initializer(self):
        return self.cell.bias_initializer

    @property
    def bias_regularizer(self):
        return self.cell.bias_regularizer

    @property
    def bias_constraint(self):
        return self.cell.bias_constraint

    def get_config(self):
        config = {'units': self.units,
                  'activation': activations.serialize(self.activation),
                  'use_bias': self.use_bias,
                  'kernel_initializer':
                      initializers.serialize(self.kernel_initializer),
                  'kernel_regularizer':
                      regularizers.serialize(self.kernel_regularizer),
                  'kernel_constraint':
                      constraints.serialize(self.kernel_constraint),
                  'bias_initializer': initializers.serialize(self.bias_initializer),
                  'bias_regularizer': regularizers.serialize(self.bias_regularizer),
                  'bias_constraint': constraints.serialize(self.bias_constraint)}
        base_config = super(ModifiedRNN, self).get_config()
        del base_config['cell']
        return dict(list(base_config.items()) + list(config.items()))

